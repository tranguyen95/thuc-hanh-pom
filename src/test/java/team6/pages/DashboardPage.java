package team6.pages;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import team6.common.SoftAssertion;
import team6.helper.ReportHelper;
import team6.testcases.BaseTest;


public class DashboardPage extends PageBase {
    private String url = "https://my-learning.w3schools.com/";
    private By tutorialBtn = By.xpath("//*[@id=\"headingcircle_1\"]");
    private By HtmlCourse = By.cssSelector("#htmltext");
    private By stydyBtn = By.xpath("//*[@id=\"tutorialgotobtn\"]");
    private By settingBtn = By.xpath("//*[@id=\"navigation\"]/a[5]");
    private By firstNameInput = By.xpath("//*[@id=\"modal_first_name\"]");
    private By lastNameInput = By.xpath("//*[@id=\"modal_last_name\"]");
    private By updateBtn = By.xpath("//*[@id=\"root\"]/div[2]/div[1]/div/div/button");
    private By homeBtn = By.xpath("//*[@id=\"w3s-top-nav-bar\"]/div/div[3]/div/a");
    private By learnCSSBtn = By.xpath("//*[@id=\"main\"]/div[2]/div/div[1]/a[1]");
    private By cssQuizBtn = By.xpath("//*[@id=\"main\"]/p[13]/a");
    private By startQuizBtn = By.xpath("//*[@id=\"main\"]/div[3]/p[2]/a");
    private By crlBtn = By.xpath("//*[@id=\"label1\"]");
    WebDriver driver;
    WebDriverWait wait;
    ExtentTest test = ReportHelper.getTest();



    public DashboardPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 2);
    }

    public void openTutorial() {
        //Assert.assertTrue(verifyUrl(),"Không phải dashboard page");
        wait.until(ExpectedConditions.elementToBeClickable(tutorialBtn));
        driver.findElement(tutorialBtn).click();


    }
    public void htmlCourseClick() {
        wait.until(ExpectedConditions.elementToBeClickable(HtmlCourse));
        driver.findElement(HtmlCourse).click();

    }
    public void studyBtnClick() {
        wait.until(ExpectedConditions.elementToBeClickable(stydyBtn));
        driver.findElement(stydyBtn).click();
    }

    public void settingClick() {
        wait.until(ExpectedConditions.elementToBeClickable(settingBtn));
        driver.findElement(settingBtn).click();
    }
    public void inputName() throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(firstNameInput));
        driver.findElement(firstNameInput).sendKeys("Team");
        driver.findElement(lastNameInput).sendKeys("six");
        Thread.sleep(2000);
        driver.findElement(updateBtn).click();
    }
    public void backHome() {

        wait.until(ExpectedConditions.elementToBeClickable(homeBtn));
        driver.findElement(homeBtn).click();
    }
    public void verifyHomepage() {
        String actualUrl = driver.getCurrentUrl();
        SoftAssertion.assertEqual(actualUrl,"https://www.w3schools.com/","Verify Hompage");
    }
    public void learnCssClick() {
        wait.until(ExpectedConditions.elementToBeClickable(learnCSSBtn));
        driver.findElement(learnCSSBtn).click();

        String url = driver.getCurrentUrl();
        SoftAssertion.assertEqual(url,"https://www.w3schools.com/","Verify displayed LearnCss page");

        System.out.println("url learncssclick " + url);
    }
    public void cssQuizBtnClick() {
        wait.until(ExpectedConditions.elementToBeClickable(cssQuizBtn));
        driver.findElement(cssQuizBtn).click();
        String url = driver.getCurrentUrl();
        SoftAssertion.assertEqual(url,"https://www.w3schools.com/css/css_quiz.asp","Verify CSS quiz Page");
        System.out.println(url);
    }
    public void startQuizBtnClick() {
        wait.until(ExpectedConditions.elementToBeClickable(startQuizBtn));
        driver.findElement(startQuizBtn).click();
        SoftAssertion.assertEqual(url,"https://www.w3schools.com/css/css_quiz.asp","Verify CSS quiz Page");
    }
    public void crlQuestion() {
        wait.until(ExpectedConditions.elementToBeClickable(crlBtn));
        driver.findElement(crlBtn).click();
    }

}
